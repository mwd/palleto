import MuiAppBar from '@material-ui/core/AppBar/AppBar';
import Button from '@material-ui/core/Button';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Toolbar from '@material-ui/core/Toolbar';
import clsx from 'clsx';
import React from 'react';
import useStore from '../hooks/useStore';

const useStyles = makeStyles(({ palette }) => ({
    button: {
        '&.isActive': {
            backgroundColor: palette.action.selected,
        },
    },
    subControls: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'flex-end',
    },
}));

export default function AppBar() {
    const classes = useStyles();
    const {
        store: { controls },
        setStore,
    } = useStore();

    return (
        <MuiAppBar>
            <Toolbar>
                <Button
                    className={clsx(classes.button, {
                        isActive: controls,
                    })}
                    onClick={() => setStore({ controls: !controls })}
                >
                    controls
                </Button>
                <div className={classes.subControls}>mwd palleto</div>
            </Toolbar>
        </MuiAppBar>
    );
}

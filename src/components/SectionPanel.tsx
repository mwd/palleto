import { Fade } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import makeStyles from '@material-ui/core/styles/makeStyles';
import AppsIcon from '@material-ui/icons/Apps';
import React, { useState } from 'react';

const useStyles = makeStyles(
    ({ spacing }) => ({
        root: {
            display: 'flex',
        },
        controls: {
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 0,
        },
        content: {
            display: 'flex',
            flexGrow: 1,
            overflow: 'hidden',
            paddingLeft: spacing(2),
        },
        section: {
            minWidth: '100%',
        },
        sectionHeader: {
            marginBottom: spacing(1),
        },
        sectionContent: {},
    }),
    {
        name: 'SectionPanel',
        index: 1,
    }
);

interface SectionPanelItemProps {
    name: string;
    icon?: React.ReactElement;
    children?: any;
}
// eslint-disable-next-line @typescript-eslint/no-unused-vars
function SectionPanelItem(props: SectionPanelItemProps) {
    return null;
}

interface SectionPanelProps {
    children: React.ReactNode | React.ReactNodeArray;
}

function SectionPanel({ children }: SectionPanelProps) {
    const classes = useStyles();

    const items = React.Children.toArray(children);
    const [active, setActive] = useState(0);

    return (
        <div className={classes.root}>
            <div className={classes.controls}>
                {items.map(
                    (
                        {
                            props: { name, icon },
                        }: React.ReactElement<SectionPanelItemProps>,
                        index
                    ) => (
                        <IconButton
                            key={name}
                            onClick={() => setActive(index)}
                            title={name}
                        >
                            {icon ? React.cloneElement(icon) : <AppsIcon />}
                        </IconButton>
                    )
                )}
            </div>
            <div className={classes.content}>
                {items.map(
                    (
                        {
                            props: { name, children: content },
                        }: React.ReactElement<SectionPanelItemProps>,
                        index
                    ) => (
                        <Fade
                            // todo styling was made with Slide. Margin handling might different ... still working though /-)
                            // direction={index === active ? 'left' : 'right'}
                            // direction="right"
                            in={index === active}
                            key={name}
                        >
                            <section
                                className={classes.section}
                                style={{
                                    marginLeft: `${index === active ? 0 : -100}%`,
                                }}
                            >
                                <header className={classes.sectionHeader}>{name}</header>
                                <div className={classes.sectionContent}>{content}</div>
                            </section>
                        </Fade>
                    )
                )}
            </div>
        </div>
    );
}

SectionPanel.Item = SectionPanelItem;
export default SectionPanel;

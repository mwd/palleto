import { fade as muiFade, Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';
import clsx from 'clsx';
import React from 'react';
import useStore from '../hooks/useStore';

const useStyles = makeStyles<
    Theme,
    {
        color: string;
        style: Palleto.SwatchStyle;
        fade: number;
    }
>(
    ({ spacing, palette, transitions, shadows }) => ({
        root: {
            padding: spacing(0.5),

            '&:hover': {
                '& $swatch': {
                    zIndex: 300,
                    transform: 'scale(1.6)',
                    boxShadow: shadows[3],
                    border: `solid 1px ${muiFade(palette.background.paper, 0.6)}`,
                    opacity: 1,
                },
            },
        },
        swatch: {
            opacity: ({ fade }) => fade / 10,
            cursor: 'crosshair',
            width: ({ style }) => style.size,
            height: ({ style }) => style.size,
            backgroundColor: ({ color }) => color,
            borderRadius: ({ style: { borderRadius, type } }) =>
                type === 'circle' ? '100%' : borderRadius,
            transition: transitions.create(['transform', 'box-shadow'], {
                duration: transitions.duration.shorter,
                easing: transitions.easing.sharp,
            }),

            '&.isBase': {
                border: `solid 4px ${muiFade(palette.background.default, 0.6)}`,
            },
            '&.isSelected': {
                opacity: 1,
                boxShadow: shadows[4],
                border: `solid 2px ${muiFade(palette.background.paper, 0.4)}`,
                transform: 'scale(1.3)',
                zIndex: 200,
            },
        },
    }),
    {
        name: 'Swatch',
        index: 1,
    }
);

interface SwatchProps {
    color: string;
    isBase?: boolean;
    isSelected?: boolean;
    onSelect?: (color: string) => void;
    fade?: number;
}

export default function Swatch({
    color,
    isBase,
    isSelected,
    onSelect,
    fade = 10,
}: SwatchProps) {
    const {
        store: { swatchStyle: style },
    } = useStore();
    const classes = useStyles({ color, style, fade });
    return (
        // eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions
        <div className={classes.root} title={color} onClick={() => onSelect(color)}>
            <div
                className={clsx(classes.swatch, {
                    isBase,
                    isSelected,
                })}
            />
        </div>
    );
}

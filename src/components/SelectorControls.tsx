import makeStyles from '@material-ui/core/styles/makeStyles';
import TextField from '@material-ui/core/TextField';
import React from 'react';
import useStore from '../hooks/useStore';

const useStyles = makeStyles(({ spacing }) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: spacing(0, -1),
    },
    control: {
        margin: spacing(0.5, 1),
        flexBasis: '20%',
        flexGrow: 1,
    },
}));

export default function SelectorControls() {
    const classes = useStyles();
    const {
        store: { selectorSettings },
        setStore,
    } = useStore();

    const handleChange = ({ target: { name, value } }) => {
        setStore({
            selectorSettings: {
                ...selectorSettings,
                [name]: parseInt(value, 10),
            },
        });
    };

    return (
        <div className={classes.root}>
            {Object.entries(selectorSettings).map(([key, value]) => (
                <TextField
                    className={classes.control}
                    key={key}
                    name={key}
                    label={key}
                    type="number"
                    value={value}
                    onChange={handleChange}
                />
            ))}
        </div>
    );
}

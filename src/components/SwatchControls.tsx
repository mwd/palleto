import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import makeStyles from '@material-ui/core/styles/makeStyles';
import TextField from '@material-ui/core/TextField';
import React from 'react';
import useStore from '../hooks/useStore';

const useStyles = makeStyles(({ spacing }) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: spacing(0, -0.5),
    },
    control: {
        margin: spacing(0.5),
        flexBasis: '20%',
        flexGrow: 1,
    },
}));

export default function SwatchControls() {
    const classes = useStyles();
    const {
        store: { swatchStyle },
        setStore,
    } = useStore();

    const handleChange = ({ target: { name, value, type } }: any) => {
        setStore({
            swatchStyle: {
                ...swatchStyle,
                [name]: type === 'number' ? parseInt(value, 10) : value,
            },
        });
    };

    return (
        <div className={classes.root}>
            <TextField
                className={classes.control}
                name="size"
                label="size"
                type="number"
                value={swatchStyle.size}
                onChange={handleChange}
            />
            <Select
                className={classes.control}
                name="type"
                label="type"
                value={swatchStyle.type}
                onChange={handleChange}
            >
                <MenuItem value="circle">circle</MenuItem>
                <MenuItem value="square">square</MenuItem>
            </Select>
            <TextField
                className={classes.control}
                name="borderRadius"
                label="borderRadius"
                type="number"
                value={swatchStyle.borderRadius}
                onChange={handleChange}
                disabled={swatchStyle.type === 'circle'}
                inputProps={{
                    min: 0,
                    max: swatchStyle.size / 2,
                }}
            />
            <TextField
                className={classes.control}
                name="fade"
                label="fade"
                type="number"
                value={swatchStyle.fade}
                onChange={handleChange}
                inputProps={{
                    min: 0,
                    max: 10,
                }}
            />
        </div>
    );
}

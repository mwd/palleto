import { Button } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import TripOriginIcon from '@material-ui/icons/TripOrigin';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import Color from 'color';
import React, { useState } from 'react';
import useStore from '../hooks/useStore';
import SectionPanel from './SectionPanel';
import Swatch from './Swatch';

const useStyles = makeStyles(
    ({ spacing, palette }) => ({
        root: {
            borderBottom: `solid 1px ${palette.divider}`,
            paddingBottom: spacing(3),
            marginBottom: spacing(3),
        },
        selection: {
            display: 'flex',
            minHeight: spacing(5),
        },
        control: {
            borderTop: `solid 1px ${palette.divider}`,
            paddingTop: spacing(3),
            marginTop: spacing(3),
        },
        controlButtons: {
            '& > *:not(:first-child)': {
                marginTop: spacing(1),
            },
        },
        json: {
            maxHeight: spacing(40),
            overflowY: 'auto',
            whiteSpace: 'pre',
            fontFamily: 'monospace',
            marginBottom: spacing(2),
        },
    }),
    {
        name: 'Selection',
        index: 1,
    }
);

export default function Selection() {
    const { store, setStore } = useStore();
    const classes = useStyles();

    const [selected, setSelected] = useState<null | any>();

    const handleDelete = (color: string) => {
        const nextSelection = [...store.selection];
        const index = nextSelection.indexOf(color);
        nextSelection.splice(index, 1);

        setStore({
            selection: nextSelection,
        });
        setSelected(nextSelection.length ? nextSelection[index ? index - 1 : 0] : null);
    };

    const renderSwatches = () =>
        store.selection.map(color => (
            <Swatch
                key={color}
                color={color}
                onSelect={(editColor: string) =>
                    setSelected(selected === editColor ? null : editColor)
                }
                isSelected={selected === color}
            />
        ));

    return (
        <div className={classes.root}>
            <div>
                <Typography gutterBottom variant="overline">
                    Selection: {store.selection.length}
                </Typography>
            </div>
            <div className={classes.selection}>
                {store.selection.length ? renderSwatches() : 'no colors selected'}
            </div>
            <Collapse in={selected}>
                <div className={classes.control}>
                    <SectionPanel>
                        <SectionPanel.Item name="item" icon={<TripOriginIcon />}>
                            <List>
                                <ListItem>name: {selected}</ListItem>
                                <ListItem>
                                    WCAG contrast ratio:{' '}
                                    {Color(selected).contrast(Color('white'))}
                                </ListItem>
                                <ListItem>
                                    hsl:{' '}
                                    {Color(selected)
                                        .hsl()
                                        .string()}
                                </ListItem>
                                <ListItem>hex: {Color(selected).hex()}</ListItem>
                            </List>
                            <Button
                                fullWidth
                                color="secondary"
                                onClick={() => handleDelete(selected)}
                            >
                                remove
                            </Button>
                        </SectionPanel.Item>
                        <SectionPanel.Item name="selection" icon={<ViewModuleIcon />}>
                            <div className={classes.json}>
                                {JSON.stringify(store.selection, null, 4)}
                            </div>
                            <Button
                                fullWidth
                                color="primary"
                                onClick={() => setStore({ selection: [] })}
                            >
                                clear
                            </Button>
                        </SectionPanel.Item>
                    </SectionPanel>
                </div>
            </Collapse>
        </div>
    );
}

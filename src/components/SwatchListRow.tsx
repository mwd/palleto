import makeStyles from '@material-ui/core/styles/makeStyles';
import React, { useContext } from 'react';
import useStore from '../hooks/useStore';
import Swatch from './Swatch';
import { SelectorContext } from './SwatchList';

const useStyles = makeStyles(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    ({ spacing }) => ({
        root: {
            display: 'flex',
            flexDirection: 'column',
        },
        item: {},
    }),
    {
        name: 'SwatchListRow',
        index: 1,
    }
);

export default function SwatchListRow({ base, name, swatches }: Palleto.Color) {
    const classes = useStyles();
    const {
        store: { selection, swatchStyle },
    } = useStore();

    const { handleSelect } = useContext(SelectorContext);

    return (
        <div className={classes.root} title={name}>
            {swatches.map(color => (
                <Swatch
                    key={color}
                    color={color}
                    isBase={color === base}
                    isSelected={selection.includes(color)}
                    onSelect={handleSelect}
                    fade={swatchStyle.fade}
                />
            ))}
        </div>
    );
}

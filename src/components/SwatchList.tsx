import makeStyles from '@material-ui/core/styles/makeStyles';
import React from 'react';
import useStore from '../hooks/useStore';
import { createColors } from '../utils/color';
import SwatchListRow from './SwatchListRow';

const useStyles = makeStyles(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    ({ spacing }) => ({
        root: {
            display: 'flex',
        },
        row: {},
    }),
    {
        name: 'SwatchList',
        index: 1,
    }
);

export const SelectorContext = React.createContext<null | any>(null);

export default function SwatchList() {
    const classes = useStyles();
    const {
        store: { selectorSettings, selection },
        setStore,
    } = useStore();

    const colors = createColors(selectorSettings);

    const handleSelect = color => {
        const nextSelection = [...selection];

        if (nextSelection.includes(color)) {
            nextSelection.splice(nextSelection.indexOf(color), 1);
        } else {
            nextSelection.push(color);
        }

        setStore({
            selection: nextSelection,
        });
    };

    return (
        <SelectorContext.Provider value={{ handleSelect }}>
            <div className={classes.root}>
                {colors.map(color => (
                    <SwatchListRow key={color.name} {...color} />
                ))}
            </div>
        </SelectorContext.Provider>
    );
}

import { Collapse, Theme, ThemeProvider } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import makeStyles from '@material-ui/core/styles/makeStyles';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import ColorLensIcon from '@material-ui/icons/ColorLens';
import React, { useState } from 'react';
import { hot } from 'react-hot-loader/root';
import StoreContext from '../contexts/StoreContext';
import AppBar from './AppBar';
import SectionPanel from './SectionPanel';
import Selection from './Selection';
import SelectorControls from './SelectorControls';
import SwatchControls from './SwatchControls';
import SwatchList from './SwatchList';

const theme: Theme = createMuiTheme({
    palette: {
        primary: {
            main: '#bfbfbf',
        },
        secondary: {
            main: '#808080',
        },
    },
    typography: {
        fontFamily: "'Heebo', sans-serif",
    },
    props: {
        MuiButton: {
            variant: 'contained',
            color: 'primary',
            size: 'small',
        },
    },
});

const useStyles = makeStyles(
    ({ spacing, palette }) => ({
        root: {
            padding: spacing(11, 3, 3),
        },
        controls: {
            borderBottom: `solid 1px ${palette.divider}`,
            paddingBottom: spacing(3),
            marginBottom: spacing(3),
        },
        view: {},
    }),
    { name: 'PageLayout', index: 1 }
);

const initialStore: Palleto.App = {
    app: 'mwd-palleto',
    controls: true,
    selection: [],
    handleSelection: selection => selection,
    selectorSettings: {
        hueSteps: 20,
        hueStart: 0,
        hueEnd: 360,
        lightenSteps: 5,
        darkenSteps: 5,
        hueShift: 0,
        luminance: 50,
        saturation: 100,
    },
    swatchStyle: {
        borderRadius: 3,
        size: 30,
        type: 'circle',
        fade: 8,
    },
};

const storageKey = 'mwd-palleto';

// hot(App) is only required for the root component.
export default hot(function App() {
    const classes = useStyles();

    const [store, setInnerStore] = useState<Palleto.App>(
        JSON.parse(localStorage.getItem(storageKey)) || initialStore
    );

    const setStore = (partial: Partial<Palleto.App>) => {
        const nextStore = {
            ...store,
            ...partial,
        };
        setInnerStore(nextStore);
        localStorage.setItem(storageKey, JSON.stringify(nextStore));
    };

    return (
        <StoreContext.Provider value={{ store, setStore }}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <AppBar />

                <div className={classes.root}>
                    <Collapse in={store.controls} collapsedHeight={0}>
                        <div className={classes.controls}>
                            <SectionPanel>
                                <SectionPanel.Item name="color" icon={<ColorLensIcon />}>
                                    <SelectorControls />
                                </SectionPanel.Item>
                                <SectionPanel.Item
                                    name="swatch display settings"
                                    icon={<CheckBoxOutlineBlankIcon />}
                                >
                                    <SwatchControls />
                                </SectionPanel.Item>
                                <SectionPanel.Item name="pattern creator">
                                    ...
                                </SectionPanel.Item>
                            </SectionPanel>
                        </div>
                    </Collapse>
                    <Selection />
                    <div className={classes.view}>
                        <SwatchList />
                    </div>
                </div>
            </ThemeProvider>
        </StoreContext.Provider>
    );
});

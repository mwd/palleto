import React from 'react';

export default React.createContext<null | {
    store: Palleto.App;
    setStore: (partial: Partial<Palleto.App>) => void;
}>(null);

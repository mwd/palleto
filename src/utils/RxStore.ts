import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, map, tap } from 'rxjs/operators';

interface RxStoreI<S> {
    getSnapshot: () => S;
    getState: () => Observable<S>;
    getPartial: <K extends keyof S>(key: K) => Observable<S[K]>;
    setState: (data: Partial<S>) => void;
}

export default class RxStore<S> implements RxStoreI<S> {
    private store$: BehaviorSubject<any>;

    private identifier?: string = 'app-store';

    public constructor(initialData: S, identifier?: string) {
        if (identifier) this.identifier = identifier;

        //
        const storage = JSON.parse(localStorage.getItem(identifier));
        // eslint-disable-next-line no-console
        console.info('RxStore: created', this.identifier, storage, initialData);
        this.store$ = new BehaviorSubject(storage || initialData);
    }

    public getSnapshot() {
        return this.store$.getValue();
    }

    public getState() {
        return this.store$.pipe(distinctUntilChanged());
    }

    public getPartial<K extends keyof S>(key: K): Observable<S[K]> {
        return this.store$.pipe(
            map(data => data[key]),
            distinctUntilChanged()
        );
    }

    public setState(data) {
        const nextData = {
            ...this.getSnapshot(),
            ...data,
        };

        this.store$.next(nextData);
        localStorage.setItem(this.identifier, JSON.stringify(this.getSnapshot()));
    }
}

import { darken, lighten } from '@material-ui/core';
import Color from 'color';

// eslint-disable-next-line import/prefer-default-export
export function createColors({
    hueSteps,
    darkenSteps,
    hueEnd,
    hueShift,
    hueStart,
    lightenSteps,
    luminance,
    saturation,
}: Palleto.ColorSelectorSettings): Palleto.Color[] {
    const sets = [];
    const hueProcessStep = Math.round((hueEnd - hueStart) / hueSteps);

    for (
        let hue = hueStart + hueShift;
        hue <= hueEnd - hueProcessStep + hueShift;
        hue += hueProcessStep !== Infinity && hueProcessStep > 0 ? hueProcessStep : hueEnd
    ) {
        const color = Color([hue, saturation, luminance], 'hsl');
        const baseColor = color.rgb().string();

        const swatches = [];

        // darken
        const darkenStep = 1 / darkenSteps;
        for (let d = 1; d > darkenStep; d -= darkenStep) {
            swatches.push(darken(baseColor, d));
        }
        // base color
        swatches.push(baseColor);
        // lighten
        const lightenStep = 1 / lightenSteps;
        for (let l = lightenStep; l < 1; l += lightenStep) {
            swatches.push(lighten(baseColor, l));
        }

        // remove black and white
        // swatches = swatches.filter(c => {
        //     const colorObject = decomposeColor(c);
        //     return !(colorObject.values[2] === 0 || colorObject.values[2] === 100);
        // });

        sets.push({
            name: `hue (${hue})`,
            base: baseColor,
            hsl: color,
            swatches,
        });
    }
    return sets;
}

// function hslArray(hsl: string): [number, number, number] {
//     // @ts-ignore
//     const a: [string, string, string] = hsl
//         .replace('hsl(', '')
//         .replace(/[,%]/g, '#')
//         .replace(')', '')
//         .replace(/\s+/g, '')
//         .split(/#+/, 3);
//     // @ts-ignore
//     return a.map(value => parseInt(value, 10));
// }

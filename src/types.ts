// eslint-disable-next-line @typescript-eslint/no-namespace

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Palleto {
    export interface App {
        app: string;
        controls: boolean;
        selectorSettings: ColorSelectorSettings;
        selection: string[];
        handleSelection: (selection) => void;
        swatchStyle: SwatchStyle;
    }

    export interface ColorSelectorSettings {
        darkenSteps: number;
        hueEnd: number;
        hueStart: number;
        hueShift: number;
        hueSteps: number;
        lightenSteps: number;
        luminance: number;
        saturation: number;
    }

    export interface Color {
        name: string;
        base: string; // TODO refactor to array
        color: any;
        swatches: string[];
    }

    export interface SwatchStyle {
        size: number;
        borderRadius: number;
        type: 'circle' | 'square';
        fade: number;
    }
}

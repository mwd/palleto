module.exports = {
    presets: ['react', 'typescript'],
    options: {
        buildDir: 'public',
        title: 'mwd palleto',
    },
    addons: {
        eslint: {
            rules: {
                'prettier/prettier': 'warn',
                'spaced-comment': 'off',
                'import/order': 'warn',
                '@typescript-eslint/ban-ts-ignore': 'off',
            },
        },
    },
};
